# Gotta write a readme
## For now:
If you know JS and want to use this:
- Rename ```secret.demo.js``` to ```secret.js```, put login details in there
- Change the ```rootDir``` function to fit your structure better, parameterisation should not be hard, though I will most likely make it available easier in the future.
- Can also rewrite ```title``` function to whatever is better. (both of these are in index.ts)
- Schedule the task if you want, there is an npm script (```start:headless```) that can be easily used in a cron task. (I use ```0 * * * * cd path && npm run start:headless >> ".logs/$(date +"%Y_%m_%d_%I_%M_%p").log" 2>&1```)