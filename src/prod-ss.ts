import { uniRequest, URLs, forms, RequestTransform } from "./helpers";
import { Class } from "./classes";

// prettier-ignore
let timetableDay = str =>
  (/ctl00_Content_ctlTimetableMain_(Mon|Tue|Wed|Thu|Fri)DayCol_Body/.exec(str) || [null, null])[1];

export let timetable = async (username: string, password: string) => {
  let prodGet = (await uniRequest({ uri: URLs.prodSS })) as RequestTransform;
  let login = { ...forms.timetable };
  Object.values(prodGet.$("#aspnetForm").serializeArray()).map(
    v => (login[v.name] = v.value)
  );
  login = {
    ...login,
    ctl00$Content$txtUserName$txtText: username,
    ctl00$Content$txtPassword$txtText: password
  };

  let timetable = (await uniRequest({
    uri: prodGet.request.uri.href,
    method: "POST",
    form: login,
    followRedirects: true
  })) as RequestTransform;

  let classes: Class[] = [];
  timetable.$(".cssClassContainer").map((i, v) => {
    let $v = timetable.$(v);
    classes = [
      ...classes,
      {
        times: {
          start: $v.find(".cssHiddenStartTm").attr("value"),
          end: $v.find(".cssHiddenEndTm").attr("value")
        },
        day: timetableDay($v.attr("id")),
        name: $v
          .find(".cssTtableHeaderPanel")
          .text()
          .split("\t")
          .filter(v => v != "\n" && v)[0]
          .replace("\n", ""),
        // prettier-ignore
        type: (_ => _.slice(0, _.indexOf(" ")))
          ($v.find(".cssTtableClsSlotWhat").text().trim())
      }
    ];
  });
  return classes;
};
