import { uniRequest, RequestTransform } from "./helpers";
import { LMSSubject, EchoLecture, Class } from "./classes";

let availableFilter = ({ lesson }) =>
  lesson &&
  lesson.video &&
  lesson.video.media &&
  lesson.video.media.media &&
  lesson.video.media.media.current;

let lectureMap = ({ lesson }) => ({
  timing: {
    start: new Date(lesson.lesson.timing.start),
    end: new Date(lesson.lesson.timing.end)
  },
  // TODO: add secondaryFiles later
  videos: lesson.video.media.media.current.primaryFiles.map(
    ({ s3Url, width, height, size }) => ({
      url: s3Url,
      width,
      height,
      size: size / 1024 / 1024
    })
  )
});

let weekdays = [undefined, "Mon", "Tue", "Wed", "Thu", "Fri"];

let lectureTimeFormatter = (date: Date) =>
  new Date(+date - 5 * 60 * 1000)
    .toLocaleTimeString("en-AU", {
      hour: "numeric",
      minute: "numeric",
      hour12: true
    })
    .toLowerCase()
    .replace(" ", "");

export let processLTI = async (
  subject: LMSSubject
): Promise<{ uri: string; $: CheerioStatic }> => {
  console.log(subject.uri);
  let echo = await uniRequest({ uri: subject.uri });
  let form = {};
  //prettier-ignore
  echo.$("form").serializeArray().map(v => (form[v.name] = v.value));
  let echoList = await uniRequest({
    uri: echo.$("form").attr("action"),
    method: "POST",
    form,
    followRedirects: true
  });
  subject.echoId = /echo360.org.au\/section\/(.+)\/home/.exec(
    echoList.request.uri.href
  )[1];
  console.log(subject.echoId);
  return { uri: echoList.request.uri.href, $: echoList.$ };
};

export let loadSubject = async (subject: LMSSubject) => {
  let list = (await uniRequest({
    uri: `https://echo360.org.au/section/${subject.echoId}/syllabus`
  })) as RequestTransform;
  let availableLectures = list.body.data
    .filter(availableFilter)
    .map(lectureMap) as EchoLecture[];
  return availableLectures;
};

export let lectureList = async (
  subject: LMSSubject,
  classes: Class[]
): Promise<EchoLecture[]> =>
  (await loadSubject(subject)).filter(
    // Filter subjects by whether or not the timetable from prod.ss has a class at the same time + day
    val =>
      !!classes.find(
        v =>
          v.type == "Lecture" &&
          v.day == weekdays[val.timing.start.getDay()] &&
          v.name == subject.name &&
          lectureTimeFormatter(val.timing.start) == v.times.start
      )
  );
