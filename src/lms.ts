import * as request from "request-promise-native";
import * as cheerio from "cheerio";
import { uniRequest, RequestTransform, URLs, forms } from "./helpers";
import { LMSSubject } from "./classes";

let ExtractLMSSubjectId = str => (/id=(_\d+_\d?)/.exec(str) || [null, null])[1];

let parseSubjectCode = str => /(\w{4}\d{5})_(\d{4})_SM(\d)/.exec(str);

export let login = async (
  username: string,
  password: string
): Promise<boolean> => {
  let loginGet = await uniRequest({ uri: URLs.lmsFetch });
  let loginPost = await uniRequest({
    uri: URLs.lmsLogin,
    method: "POST",
    // prettier-ignore
    form: { ...forms.login, user_id: username, password },
    followRedirects: true
  });
  return true;
  // TODO: Find out somehow if authenticated or not
};

export let parseLMS = async (): Promise<LMSSubject[]> => {
  let subjects = (await uniRequest({
    uri: URLs.lmsSubjects,
    method: "POST",
    form: forms.subjects
  })) as RequestTransform;
  let subjectList: LMSSubject[] = [];
  // This map is disgusting. Thanks cheerio!
  let code;
  let subject = {
    name: parseSubjectCode(code) ? parseSubjectCode(code)[1] : undefined
  };
  subjects
    .$("a")
    .filter((i, v) => !!subjects.$(v).text())
    .map((i, v) => {
      let $v = subjects.$(v);
      let code = $v.text();

      let [del, name, year, semester] = parseSubjectCode(code);
      let subject = {
        lmsId: ExtractLMSSubjectId($v.attr("href")),
        echoId: undefined,
        name,
        year: +year,
        semester: +semester as 1 | 2, // We know there won't be a 3
        uri: URLs.lmsLTI(ExtractLMSSubjectId($v.attr("href")))
      };
      subjectList.push(subject);
    });
  return subjectList.filter(v => v.semester == 2);
};
