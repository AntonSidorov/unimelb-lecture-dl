export class Class {
  times: { start: string; end: string };
  name: string;
  type: string;
  day: string;
}

export class LMSSubject {
  name: string;
  year: number;
  // Summer subjects will be added later
  semester: 1 | 2;
  uri: string;
  lmsId: string;
  echoId: string;
}

export class EchoLecture {
  timing: { start: Date; end: Date };
  videos: [{ url: string; width: number; height: number; size: number }];
}
