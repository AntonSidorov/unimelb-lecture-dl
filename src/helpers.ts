import * as request from "request-promise-native";
import * as r from "request";
import * as cheerio from "cheerio";

export const URLs = {
  lmsFetch:
    "https://app.lms.unimelb.edu.au/webapps/portal/execute/tabs/tabAction?tab_tab_group_id=_115_1",
  lmsLogin: "https://app.lms.unimelb.edu.au/webapps/login/",
  lmsSubjects:
    "https://app.lms.unimelb.edu.au/webapps/portal/execute/tabs/tabAction",
  lmsLTI: id =>
    `https://app.lms.unimelb.edu.au/webapps/blackboard/execute/blti/launchPlacement?blti_placement_id=_42_11&course_id=${id}`,
  prodSS:
    "https://prod.ss.unimelb.edu.au/student/SM/StudentTtable10.aspx?r=%23UM.STUDENT.APPLICANT&f=%24S1.EST.TIMETBL.WEB"
};
export let forms = {
  login: {
    user_id: undefined,
    password: undefined,
    login: "Login",
    action: "login",
    new_loc: ""
  },
  subjects: {
    action: "refreshAjaxModule",
    modId: "_4_1",
    tabId: "_1618465_1",
    tab_tab_group_id: "_115_1"
  },
  timetable: {
    ctl00$Content$txtPassword$txtText: undefined,
    ctl00$Content$txtUserName$txtText: undefined,
    __EVENTARGUMENT: "",
    __EVENTTARGET: "ctl00$Content$cmdLogin"
  }
};

export let transform = (_, r: r.Response) => {
  let isJson = r.headers["content-type"].indexOf("application/json") > -1;

  let _$;
  let otherProps = isJson
    ? { body: JSON.parse(r.body) }
    : {
        _$,
        get $() {
          if (!_$) _$ = cheerio.load(r.body);
          return _$;
        }
      };

  return {
    ...r,
    ...otherProps
  } as RequestTransform;
};

export const jar = request.jar();

let reqDefaults = {
  uri: "",
  method: "GET",
  jar,
  followRedirects: false
};

export let uniRequest = (opts: {
  uri?: string;
  method?: "GET" | "POST";
  binary?: boolean;
  jar?: r.CookieJar;
  followRedirects?: boolean;
  form?: any;
}) => {
  let { uri, method, followRedirects, form, binary } = {
    ...reqDefaults,
    ...opts
  };

  let options: request.OptionsWithUri = {
    uri,
    method,
    transform,
    jar,
    followAllRedirects: followRedirects
  };
  if (binary) options.encoding = "binary";
  if (form) options.form = form;
  return (method == "GET" ? request.get : request.post)(options);
};

export interface RequestTransform extends r.Response {
  _$?: CheerioStatic;
  readonly $?: CheerioStatic;
}
