import * as fs from "fs";
import * as req from "request";
import { login, parseLMS } from "src/lms";
import { timetable } from "./src/prod-ss";
import user from "./secret.js";
import { uniRequest, jar } from "src/helpers";
import { processLTI, loadSubject, lectureList } from "src/echo";
import { EchoLecture } from "./src/classes";

// Path to save, change it to whatever you want.
const rootDir = subject =>
  `/Users/antonsidorov/Documents/Education/Uni/${subject}/Lectures/Recordings`;

let ensureExists = dir => (fs.existsSync(dir) ? true : fs.mkdirSync(dir));

let main = async () => {

  await login(user.username, user.password);
  let classes = await timetable(user.username, user.password);
  let lmsSubjects = await parseLMS();

  // Change this for custom titles
  let title = (lecture: EchoLecture, i: number) =>
    `Lecture ${i + 1} - ${lecture.timing.start.toLocaleDateString("en-AU", {
      month: "short",
      day: "numeric"
    })}`;

  for (let sub of lmsSubjects) {
    ensureExists(rootDir(sub.name));
    await processLTI(sub);
    let list = await lectureList(sub, classes);
    for (let [i, lecture] of list.entries()) {
      if (fs.existsSync(`${rootDir(sub.name)}/${title(lecture, i)}.mp4`)) {
        console.log(
          `Lecture ${i + 1} for ${sub.name} already saved. Skipping!`
        );
        continue;
      }
      console.log(`Downloading ${sub.name} - Lecture ${i + 1}`);
      req(lecture.videos[1].url, { jar }).pipe(
        fs.createWriteStream(`${rootDir(sub.name)}/${title(lecture, i)}.mp4`)
      );
    }
  }

};

main();
